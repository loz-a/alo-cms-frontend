import React from 'react'
import { Router, Route, IndexRoute } from 'react-router'
import history from 'common/history'

import PageNotFound from 'common/components/PageNotFound'
import AppContainer from 'languages/containers/AppContainer'
import ListContainer from 'languages/containers/Languages/ListContainer'
import AddItemContainer from 'languages/containers/Languages/AddItemContainer'
import EditItemContainer from 'languages/containers/Languages/EditItemContainer'
import DeleteItemContainer from 'languages/containers/Languages/DeleteItemContainer'
import AddDisplayValueContainer from 'languages/containers/Languages/DisplayValues/AddDisplayValueContainer'
import config from 'languages/config.json'

export default (
  <Router history={history}>
    <Route path={config.routes.languages.index} component={AppContainer}>
      <IndexRoute component={ListContainer} />
      <Route component={ListContainer}>
        <Route path={config.routes.languages.add} component={AddItemContainer} />
        <Route path={config.routes.languages.delete} component={DeleteItemContainer} />
      </Route>
      <Route path={config.routes.languages.edit} component={EditItemContainer}>
        <Route path={config.routes.languages.displayValues.add} component={AddDisplayValueContainer} />
      </Route>
    </Route>
    <Route path="*" component={PageNotFound} />
  </Router>
)
