import api from 'languages/api'
import * as constants from 'languages/constants'
import { checkHttpStatus } from 'common/utils'
import { CALL_API } from 'common/middlewares/api'
import history from 'common/history'
import config from 'languages/config.json'

export const fetchLanguages = () => ({
  [CALL_API]: {
    types: [
      constants.FETCH_LANGUAGES_REQUEST,
      constants.FETCH_LANGUAGES_SUCCESS,
      constants.FETCH_LANGUAGES_FAILURE
    ],
    apiResolver: api.fetchLanguages().then(checkHttpStatus)
  }
})

export const toggleEnable = (languageId, isEnable) => ({
  [CALL_API]: {
    types: [
      constants.TOGGLE_ENABLE_LANGUAGE_REQUEST,
      constants.TOGGLE_ENABLE_LANGUAGE_SUCCESS,
      constants.TOGGLE_ENABLE_LANGUAGE_FAILURE
    ],
    requestPayload: { languageId },
    apiResolver: api.toggleEnable(languageId, isEnable).then(checkHttpStatus)
  }
})

export const addLanguage = (data) => ({
  [CALL_API]: {
    types: [
      constants.ADD_LANGUAGE_REQUEST,
      constants.ADD_LANGUAGE_SUCCESS,
      constants.ADD_LANGUAGE_FAILURE
    ],
    apiResolver: api
      .saveLanguage(data)
      .then(checkHttpStatus)
      .then((response) => {
        const isSuccess = response.status == 200 && !('validateErrors' in response.data)
        if (isSuccess) history.push(config.routes.languages.index)
        return response
      })
  }
})

export const editLanguage = (language) => ({
  [CALL_API]: {
    types: [
      constants.EDIT_LANGUAGE_REQUEST,
      constants.EDIT_LANGUAGE_SUCCESS,
      constants.EDIT_LANGUAGE_FAILURE
    ],
    apiResolver: api
      .saveLanguage(language)
      .then(checkHttpStatus)
  }
})

export const deleteLanguage = (languageId) => ({
  [CALL_API]: {
    types: [
      constants.DELETE_LANGUAGE_REQUEST,
      constants.DELETE_LANGUAGE_SUCCESS,
      constants.DELETE_LANGUAGE_FAILURE
    ],
    apiResolver: api
      .deleteLanguage(languageId)
      .then(checkHttpStatus)
      .then((response) => {
        const isSuccess = response.status == 200
        if (isSuccess) history.replace(config.routes.languages.index)
        return response
      })
  }
})

export const clearValidateErrors = () => ({
  type: constants.CLEAR_VALIDATE_ERRORS
})
