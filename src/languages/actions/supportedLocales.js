import api from 'languages/api'
import * as constants from 'languages/constants'
import { checkHttpStatus } from 'common/utils'
import { CALL_API } from 'common/middlewares/api'

export const fetchSupportedLocales = () => ({
  [CALL_API]: {
    types: [
      constants.FETCH_SUPPORTED_LOCALES_REQUEST,
      constants.FETCH_SUPPORTED_LOCALES_SUCCESS,
      constants.FETCH_SUPPORTED_LOCALES_FAILURE
    ],
    apiResolver: api.fetchSupportedLocales().then(checkHttpStatus)
  }
})
