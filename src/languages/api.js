import { httpClient } from 'common/utils'
import config from 'languages/config.json'

export default {
  fetchLanguages() {
    return httpClient().get(config.url.languages)
  },

  saveLanguage(languageData) {
    const normalizedData = { ...languageData, is_enable: (+languageData.isEnable) + ''}
    delete normalizedData.isEnable

    return httpClient().post(config.url.languageSave, normalizedData)
  },

  deleteLanguage(languageId) {
    return httpClient().post(config.url.languageDelete, {languageId})
  },

  toggleEnable(languageId, isEnable) {
    return httpClient().post(config.url.toggleEnable, {languageId, isEnable})
  },

  fetchSupportedLocales() {
    return httpClient().get(config.url.supportedLocales)
  }
}
