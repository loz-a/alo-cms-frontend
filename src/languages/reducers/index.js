import { combineReducers } from 'redux'
import appConfig, * as fromAppConfig from 'app-config/reducers'
import jwt, * as fromJwt from 'jwt/reducers'
import languages, * as fromLanguages from './languages'
import supportedLocales, * as fromSupportedLocales from './supportedLocales'

export default combineReducers({
  appConfig,
  jwt,
  languages,
  supportedLocales
})

export const isAppConfigErrorLoaded = (state) => fromAppConfig.isErrorLoaded(state)

export const isAppConfigLoading = (state) => fromAppConfig.isLoading(state)

export const getAppConfig = (state) => fromAppConfig.getData(state)


export const getUsername = (state) => fromJwt.getUsername(state)

export const isAuthenticated = (state) => fromJwt.isAuthenticated(state)


export const getLanguageById = (state, id) => fromLanguages.getLanguageById(state, id)

export const getLanguages = (state) => fromLanguages.getLanguages(state)

export const getLanguagesLocales = (state) => fromLanguages.getLocales(state)

export const isLanguagesLoaded = (state) => fromLanguages.isLoaded(state)

export const isLanguagesLoading = (state) => fromLanguages.isLoading(state)

export const getLanguagesErrorMessage = (state) => fromLanguages.getErrorMessage(state)

export const getLanguagesValidateErrors = (state) => fromLanguages.getValidateErrors(state)


export const getSupportedLocales = (state) => fromSupportedLocales.getLocales(state)

export const isSupportedLocalesLoading = (state) => fromSupportedLocales.isLoading(state)

export const isSupportedLocalesLoaded = (state) => fromSupportedLocales.isLoaded(state)

export const getSupportedLocalesErrorMessage = (state) => fromSupportedLocales.getErrorMessage(state)
