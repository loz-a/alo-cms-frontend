import * as constants from 'languages/constants'
import { normalize } from 'normalizr'
import languagesSchema from 'languages/store/schema'

const INIT_STATE = {
  byId: {},
  localesById: {},
  displayValuesById: {},
  loading: false,
  loaded: false,
  errorMessage: '',
  validateErrors: {}
}

function populateLanguage(state, payload) {
  return {
    byId: {
      ...state.byId,
      [payload.language.id]: {
        id: parseInt(payload.language.id),
        'language': payload.language.language,
        'locale': parseInt(payload.language.locale.id),
        'isEnable': payload.language.is_enable,
        'description': payload.language.description,
        'displayValues': {}
      }
    },
    localesById: {
      ...state.localesById,
      [payload.language.locale.id]: {
        'id': parseInt(payload.language.locale.id),
        'value': payload.language.locale.value
      }
    }
  }
}


function deleteLanguage(state, payload) {
  const { byId, displayValuesById, localesById } = state
  const deletedLanguage = state.byId[payload.language.id]
  return {
    byId: Object.keys(byId).reduce((acc, id) => {
      if (deletedLanguage.id != id) acc[id] = { ...byId[id] }
      return acc
    }, {}),

    displayValuesById: deletedLanguage.displayValues.length
      ? (Object.keys(displayValuesById).reduce((acc, id) => {
          const displayValuesIds = Object.keys(deletedLanguage.displayValues)
            // another variant (displayValuesIds.indexOf(id) < 0)
            if (!displayValuesIds.includes(id)) acc[id] = { ...displayValuesById[id] }
            return acc
        }, {}))
      : displayValuesById,

    localesById: Object.keys(localesById).reduce((acc, id) => {
      if (deletedLanguage.locale != id) acc[id] = { ...localesById[id] }
      return acc
    }, {})
  }
}


export default function(state = INIT_STATE, action) {

  switch (action.type) {
    case constants.FETCH_LANGUAGES_REQUEST:
      return { ...state, loading: true }

    case constants.TOGGLE_ENABLE_LANGUAGE_REQUEST:
    case constants.ADD_LANGUAGE_REQUEST:
    case constants.EDIT_LANGUAGE_REQUEST:
    case constants.DELETE_LANGUAGE_REQUEST:
      return { ...state, validateErrors: INIT_STATE.validateErrors, loading: true }

    case constants.FETCH_LANGUAGES_SUCCESS:
      const entities = normalize(action.payload, languagesSchema).entities

      return {
        ...state,
        byId: entities.languages,
        localesById: entities.locales,
        displayValuesById: entities.display_values,
        loading: INIT_STATE.loading,
        loaded: true,
        errorMessage: INIT_STATE.errorMessage
      }

    case constants.FETCH_LANGUAGES_FAILURE:
      return {
        ...state,
        loading: INIT_STATE.loading,
        loaded: INIT_STATE.loaded,
        errorMessage: action.payload.errorMessage
      }

    case constants.TOGGLE_ENABLE_LANGUAGE_SUCCESS:
      return {
        ...state,
        byId: {
          ...state.byId,
          [action.payload.language.id]: {
            ...state.byId[action.payload.language.id],
            isEnable: action.payload.language.isEnable
          }
        },
        loading: INIT_STATE.loading
      }

    case constants.TOGGLE_ENABLE_LANGUAGE_FAILURE:
    return {
      ...state,
      loading: INIT_STATE.loading,
      errorMessage: action.payload.errorMessage
    }

    case constants.ADD_LANGUAGE_SUCCESS:
      if (action.payload.validateErrors) return {
        ...state,
        validateErrors: action.payload.validateErrors,
        loading: INIT_STATE.loading
      }

      return {
        ...state,
        ...populateLanguage(state, action.payload),
        validateErrors: INIT_STATE.validateErrors,
        loading: INIT_STATE.loading
      }

    case constants.ADD_LANGUAGE_FAILURE:
      return {
        ...state,
        loading: INIT_STATE.loading,
        errorMessage: action.payload.errorMessage,
        validateErrors: INIT_STATE.validateErrors
      }

    case constants.EDIT_LANGUAGE_SUCCESS:
      return {
        ...state,
        loading: INIT_STATE.loading
      }

    case constants.EDIT_LANGUAGE_FAILURE:
      return {
        ...state,
        loading: INIT_STATE.loading
      }

    case constants.DELETE_LANGUAGE_SUCCESS:
      return {
        ...state,
        ...deleteLanguage(state, action.payload),
        loading: INIT_STATE.loading
      }

    case constants.CLEAR_VALIDATE_ERRORS:
      return {
        ...state,
        validateErrors: INIT_STATE.validateErrors
      }

    default:
      return state
  }
}

export const getLanguageById = (state, id) => {
  const language = state.languages.byId[id]
  if (!language) return null

  return {
    ...language,
    locale: getLocaleById(state, language.locale),
    displayValues: getDisplayValuesByIds(state, language.displayValues)
  }
}

export const getLanguages = (state) => {
  return Object.keys(state.languages.byId).reduce((acc, id) => {
    acc[id] = getLanguageById(state, id)
    return acc
  }, {})
}

export const getLocales = (state) => state.languages.localesById

export const isLoaded = (state) => state.languages.loaded

export const isLoading = (state) => state.languages.loading

export const getErrorMessage = (state) => state.languages.errorMessage

export const getValidateErrors = (state) => state.languages.validateErrors

const getLocaleById = (state, id) => state.languages.localesById[id]

const getDisplayValuesByIds = (state, ids) => {
  return Object.keys(ids)
    .reduce((acc, id) => {
      const entity = state.languages.displayValuesById[ids[id]]
      acc[ids[id]] = {
        ...entity,
        'locale': getLocaleById(state, entity.locale)
      }
      return acc
    }, {})
}
