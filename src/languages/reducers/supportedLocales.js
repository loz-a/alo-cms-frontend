import * as constants from 'languages/constants'

const INIT_STATE = {
  byName: {},
  loading: false,
  loaded: false,
  errorMessage: ''
}

export default function(state = INIT_STATE, action) {
  switch (action.type) {
    case constants.FETCH_SUPPORTED_LOCALES_REQUEST:
      return {
        ...state,
        loading: true
      }

    case constants.FETCH_SUPPORTED_LOCALES_SUCCESS:
      return {
        byName: action.payload.locales,
        loading: INIT_STATE.loading,
        loaded: true
      }

    case constants.FETCH_SUPPORTED_LOCALES_FAILURE:
      return {
        ...state,
        loading: INIT_STATE.loading,
        loaded: INIT_STATE.loaded,
        errorMessage: action.payload.errorMessage
      }

      default:
        return state
    }
  }

  export const getLocales = (state) => state.supportedLocales.byName

  export const isLoading = (state) => state.supportedLocales.loading

  export const isLoaded = (state) => state.supportedLocales.loaded

  export const getErrorMessage = (state) => state.supportedLocales.errorMessage
