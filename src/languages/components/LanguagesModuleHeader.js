import React, { PropTypes } from 'react'
import ModuleHeader from 'common/components/ModuleHeader'
import withTranslate from 'app-config/decorators/withTranslate'
import withNavigation from 'app-config/decorators/withNavigation'

class LanguagesModuleHeader extends React.Component {

  componentDidMount() {
    const { navigation } = this.props
    this._iconName = (navigation.activeItem || {}).icon || ''
  }

  render() {
    const  { translate } = this.props
    const iconName = this._iconName

    return (
      <ModuleHeader
        headerText={translate('Site Languages')}
        subheaderText={translate('List of languages which available on site')}
        iconName={iconName}/>
    )
  }
}

LanguagesModuleHeader.propTypes = {
  translate: PropTypes.func.isRequired
}


export default withNavigation(withTranslate(LanguagesModuleHeader))
