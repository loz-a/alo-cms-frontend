import React, { PropTypes } from 'react'
import { Link } from 'react-router'
import { LanguagePropType } from 'languages/utils/propTypes'
import classNames from 'classnames'
import config from 'languages/config.json'
import './styles.css'

const Item = ({
  item: language,
  toggleEnable
}) => {  
  const flagCls = `${language.locale.value.split('_')[1].toLowerCase()} flag`
  const rowClas = classNames({'disabled': !language.isEnable})
  const routes = config.routes.languages

  const onChangeHandler = (evt) => {
    toggleEnable(evt.target.checked)
  }

  return (
    <tr className={rowClas}>
      <td>
        <div className="ui center aligned">
          <i className={flagCls}></i>
        </div>
      </td>
      <td>{language.language}</td>
      <td>{language.locale.value}</td>
      <td>{language.description}</td>
      <td>
        <div className="ui slider checkbox">
          <input
            type="checkbox"
            defaultChecked={language.isEnable}
            onChange={onChangeHandler}/>
          <label></label>
        </div>
      </td>
      <td className="actions">
        <div className="ui horizontal list">
          <Link className="item" to={`${routes.index}/${routes.edit.replace(':id', language.id)}`}>
            <i className="large link edit icon"></i>
            <div className="bottom aligned content">Edit</div>
          </Link>
          <Link className="item" to={`${routes.index}/${routes.delete.replace(':id', language.id)}`}>
            <i className="large link trash icon"></i>
            <div className="bottom aligned content">Delete</div>
          </Link>
        </div>
      </td>
    </tr>
  )
}


Item.propTypes = {
  item: LanguagePropType,
  toggleEnable: PropTypes.func.isRequired
}

export default Item
