import React, { PropTypes } from 'react'
import { Link } from 'react-router'
import { LanguagePropType } from 'languages/utils/propTypes'
import config from 'languages/config.json'

class DeleteItem extends React.Component {

  onSubmit = (evt) => {
    evt.preventDefault()

    const { language } = this.props
    this.props.deleteLanguage(language.id)
  }

  render() {
    const { language } = this.props
    if (!language) return null

    const flagCls = `${language.locale.value.split('_')[1].toLowerCase()} flag`

    return (
      <tr className="negative">
        <td colSpan="5">
          <span className="ui">
            <i className={flagCls}></i>
          </span>

          Are you sure you want to delete language {language.language} and locale {language.locale.value}?
        </td>

        <td className="actions">
          <div className="ui horizontal list">
            <a className="item" onClick={this.onSubmit}>
              <i className="large link red remove icon"></i>
              <div className="bottom aligned content">Yes</div>
            </a>
            <Link className="item" to={`${config.routes.languages.index}`}>
              <i className="large link undo icon"></i>
              <div className="bottom aligned content">No</div>
            </Link>
          </div>
        </td>

      </tr>
    )
  }
}

DeleteItem.propTypes = {
  language: LanguagePropType,
  deleteLanguage: PropTypes.func.isRequired
}

export default DeleteItem
