import React, { PropTypes } from 'react'
import { Link } from 'react-router'
import config from 'languages/config.json'
import SupportedLocales from 'languages/containers/Languages/SupportedLocalesContainer'
import { LocalePropType } from 'languages/utils/propTypes'
import DataValidator from 'common/decorators/DataValidator'
import validator from 'languages/validators/AddLanguage'
import './styles.css'

class AddItem extends React.Component {

  constructor(props) {
    super(props)
    this.state = this.getDefaultState()
  }

  getDefaultState() {
    return {
      language: {
        language: 'New Language',
        locale: '',
        description: '',
        isEnable: false
      },
      iconClass: 'flag icon'
    }
  }

  onIsEnableChange = (evt) => {
    this.setState({
      language: {
        ...this.state.language,
        isEnable: evt.target.checked
      }
    })
  }

  onDescriptionChange = (evt) => {
    this.setState({
      language: {
        ...this.state.language,
        description: evt.target.value
      }
    })
  }

  onLocalesChange = (locale, description) => {
    const [language, countryCode] = locale.split('_')
    this.setState({
      language: {
        ...this.state.language,
        locale,
        language,
        description: (this.state.language.description) || description,
      },
      iconClass: `${countryCode.toLowerCase()} flag`
    })
  }

  onCloseError = (evt) => {
    evt.preventDefault()
    this.setState(this.getDefaultState())
    this.props.clearErrorsMessages()
  }

  onSubmit = (evt) => {
    evt.preventDefault()

    const { locale, validate, addLanguage } = this.props
    const { language } = this.state

    if (validate(language, {locale})) addLanguage(language)
  }

  render() {
    const errorMessages = this.props.getErrorMessages()
    if (errorMessages.length) return this.renderErrors(errorMessages)

    return (
      <tr className="positive">
        <td>
          <div className="ui center aligned">
            <i className={this.state.iconClass}></i>
          </div>
        </td>
        <td>
          {this.state.language.language}
        </td>
        <td>
            <SupportedLocales onChangeHandler={this.onLocalesChange} />
        </td>
        <td>
          <div className="ui input">
            <input type="text"
              value={this.state.language.description}
              onChange={this.onDescriptionChange}
              placeholder="New Description"/>
          </div>
        </td>
        <td>
          <div className="ui toggle checkbox">
            <input type="checkbox"
              defaultChecked={this.state.language.isEnable}
              onChange={this.onIsEnableChange}/>
            <label></label>
          </div>
        </td>
        <td className="actions">
          <div className="ui horizontal list">
            <Link
              className="item" to={config.routes.languages.index}
              onClick={this.onSubmit}>
              <i className="large link thumbs up icon"></i>
              <div className="bottom aligned content">Ok</div>
            </Link>
            <Link className="item" to={`${config.routes.languages.index}`}>
              <i className="large link cancel icon"></i>
              <div className="bottom aligned content">Cancel</div>
            </Link>
          </div>
        </td>
      </tr>
    )
  }

  renderErrors(errors) {
    return (
      <tr className="negative">
        <td colSpan="6">
          <a className="close-btn" onClick={this.onCloseError}>
            <i className="cancel icon"></i> Close
          </a>
          <div className="ui list">
            {errors.map((error, idx) => (
              <div className="item" key={idx}>{error}</div>
            ))}
          </div>
        </td>
      </tr>
    )
  }

}

AddItem.propTypes = {
  addLanguage: PropTypes.func.isRequired,
  locales: PropTypes.objectOf(LocalePropType),
  validate: PropTypes.func.isRequired,
  getErrorMessages: PropTypes.func.isRequired,
  clearErrorsMessages: PropTypes.func.isRequired
}

export default DataValidator(validator)(AddItem)
