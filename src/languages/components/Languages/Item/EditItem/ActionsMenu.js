import React, { PropTypes } from 'react'
import { Link } from 'react-router'
import config from 'languages/config.json'

const ActionsMenu = () => {
  return (
    <div className="ui top attached inverted grey menu borderless">
      <Link to={`${config.routes.languages.index}`} className="item">
        <i className="large icons">
          <i className="inverted grey list icon"></i>
          <i className="inverted grey corner world icon"></i>
        </i>
        Back to languages list
      </Link>
    </div>
  )
}

export default ActionsMenu
