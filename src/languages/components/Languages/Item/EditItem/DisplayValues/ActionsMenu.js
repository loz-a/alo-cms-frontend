import React, { PropTypes } from 'react'
import { Link } from 'react-router'
import config from 'languages/config.json'
import { LanguagePropType } from 'languages/utils/propTypes'

const ActionsMenu = ({
  language
}) => {
  const routerConfig = config.routes.languages
  const uri = [
    routerConfig.index,
    routerConfig.edit.replace(':id', language.id),    
    routerConfig.displayValues.add
  ]

  return (
    <div className="ui top attached inverted grey menu borderless">
      <Link to={`${uri.join('/')}`} className="item">
          <i className="large icons">
          <i className="inverted grey translate icon"></i>
          <i className="inverted grey corner add icon"></i>
        </i>
        New Display Value
      </Link>
    </div>
  )
}

ActionsMenu.propTypes = {
  language: LanguagePropType.isRequired,
}

export default ActionsMenu
