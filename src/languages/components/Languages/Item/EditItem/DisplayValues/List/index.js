import React, { PropTypes } from 'react'
import { LanguagePropType } from 'languages/utils/propTypes'
import BaseTable from 'common/components/Table/BaseTable'
import Item from '../Item'
import ActionsMenu from '../ActionsMenu'
import AddDisplayValueContainer from 'languages/containers/Languages/DisplayValues/AddDisplayValueContainer'
import './styles.css'

class List extends BaseTable {

  render() {
    return (
      <div className="display-values-container">
        <ActionsMenu language={this.props.language} />
        {super.render()}
      </div>
    )
  }

  _getTHead = () => (
    <thead>
      <tr>
        <th className="one wide center aligned">
          <i className="large flag outline icon"></i>
        </th>
        <th>Locale</th>
        <th>Display Value</th>
        <th>Actions</th>
      </tr>
    </thead>
  )

  _getItems = () => this.props.language.displayValues

  _getItemElementPrototype = () => Item

  _shouldAddItemRender = () => {
    const { children } = this.props
    return children && children.type === AddDisplayValueContainer
  }

  _shouldDeleteItemRender = (itemId) => {
    // const { routeId, children } = this.props
    // return routeId == itemId && children && children.type === DeleteItemContainer
    return false
  }

}

List.propTypes = {
  language: LanguagePropType.isRequired,
}

export default List
