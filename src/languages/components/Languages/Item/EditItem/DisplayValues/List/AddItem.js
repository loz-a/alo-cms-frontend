import React from 'react'
import { Link } from 'react-router'
import config from 'languages/config.json'
import EnableLocales from 'languages/containers/Languages/EnableLocalesContainer'

class AddItem extends React.Component {

  constructor(props) {
    super(props)
    this.state = this.getDefaultState()
  }

  getDefaultState = () => ({
    displayValue: {
      locale: '',
      value: '',
    },
    iconClass: 'flag icon'
  })

  onDisplayValueChange = (evt) => {
    this.setState({
      displayValue: {
        ...this.state.displayValue,
        value: evt.target.value
      }
    })
  }

  onLocalesChange = (locale, description) => {
    this.setState({
      displayValue: {
        locale: locale,
        value: this.state.displayValue.value || description
      },
      iconClass: `${locale.split('_')[1].toLowerCase()} flag`
    })
  }

  onCloseError = (evt) => {
    evt.preventDefault()
    this.setState(this.getDefaultState())
    this.props.clearErrorsMessages()
  }

  render() {
    return (
      <tr className="positive">
        <td>
          <div className="ui center aligned">
            <i className={this.state.iconClass}></i>
          </div>
        </td>

        <td>
          <EnableLocales onChangeHandler={this.onLocalesChange} />
        </td>

        <td>
          <div className="ui input">
            <input type="text"
              value={this.state.displayValue.value}
              onChange={this.onDisplayValueChange}
              placeholder="New Display Value"/>
          </div>
        </td>

        <td className="actions">
          <div className="ui horizontal list">
            <Link
              className="item" to={config.routes.languages.index}
              onClick={this.onSubmit}>
              <i className="large link thumbs up icon"></i>
              <div className="bottom aligned content">Ok</div>
            </Link>
            <Link className="item" to={`${config.routes.languages.index}`}>
              <i className="large link cancel icon"></i>
              <div className="bottom aligned content">Cancel</div>
            </Link>
          </div>
        </td>
      </tr>
    )
  }
}

export default AddItem
