import React, { PropTypes } from 'react'
import { Link } from 'react-router'
import { DisplayValuePropType } from 'languages/utils/propTypes'
import config from 'languages/config.json'

const Item = ({
  item: displayValue
}) => {  
  const flagCls=`${displayValue.locale.value.split('_')[1].toLowerCase()} flag`
  const routes = config.routes.languages.displayValues

  return (
    <tr>
      <td>
        <div className="ui center aligned">
          <i className={flagCls}></i>
        </div>
      </td>
      <td>{displayValue.locale.value}</td>
      <td>{displayValue.value}</td>
      <td className="actions">
        <div className="ui horizontal list">
          {/* TODO get route from config */}
          <Link className="item" to={`${routes.index}/${routes.edit.replace(':id', displayValue.id)}`}>
            <i className="large link edit icon"></i>
            <div className="bottom aligned content">Edit</div>
          </Link>
          <Link className="item" to={`${routes.index}/${routes.delete.replace(':id', displayValue.id)}`}>
            <i className="large link trash icon"></i>
            <div className="bottom aligned content">Delete</div>
          </Link>
        </div>
      </td>
    </tr>
  )
}

Item.propTypes = {
  item: DisplayValuePropType.isRequired
}

export default Item
