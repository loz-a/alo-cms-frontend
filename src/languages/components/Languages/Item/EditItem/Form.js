import React, { PropTypes } from 'react'
import { LanguagePropType } from 'languages/utils/propTypes'

const Form = ({
  language,
  children
}) => (
  <form className="ui form edit-form">
    <div className="three fields">
      <div className="field">
        <label>Language (read only):</label>
        <input type="text" defaultValue={language.language} readOnly />
      </div>
      <div className="field">
        <label>Locale (read only):</label>
        <input type="text" defaultValue={language.locale.value} readOnly />
      </div>
      <div className="field">
        <label>Description:</label>
        <input type="text" defaultValue={language.description} />
      </div>
    </div>

    <div className="ui segment">
      <div className="field">
        <div className="ui slider checkbox">
          <input type="checkbox" name="is_enable" tabIndex="0" className="hidden"/>
          <label>Enable/Disable language</label>
        </div>
      </div>
    </div>

    <h4 className="ui dividing header">Language Display Values:</h4>

    {children}

    <button className="ui button" type="submit">Submit</button>
  </form>
)

Form.propTypes = {
  language: LanguagePropType.isRequired,
  children: PropTypes.element.isRequired
}

export default Form
