import React, { PropTypes } from 'react'
import { LanguagePropType } from 'languages/utils/propTypes'
import ActionsMenu from './ActionsMenu'
import List from './displayValues/List'
import Form from './Form'
import './styles.css'

class EditItem extends React.Component {

  render() {
    const { language, children } = this.props

    return (
      <div>
        <ActionsMenu />
        <Form language={language}>
          <List language={language}>
            {children}
          </List>
        </Form>
      </div>
    )
  }
}

EditItem.propTypes = {
  language: LanguagePropType.isRequired,
  editLanguage: PropTypes.func.isRequired
}

export default EditItem
