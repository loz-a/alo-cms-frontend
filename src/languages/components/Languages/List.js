import React, { PropTypes } from 'react'
import { LanguagePropType } from 'languages/utils/propTypes'
import ActionsMenu from './ActionsMenu'
import AddItemContainer from 'languages/containers/Languages/AddItemContainer'
import DeleteItemContainer from 'languages/containers/Languages/DeleteItemContainer'
import BaseTable from 'common/components/Table/BaseTable'
import Item from './Item'

class List extends BaseTable {

  render() {
    return (
      <div>
        <ActionsMenu />
        {super.render()}
      </div>
    )
  }

  _getTHead = () => (
    <thead>
      <tr>
        <th className="one wide center aligned">
          <i className="large flag outline icon"></i>
        </th>
        <th>Language</th>
        <th>Locale</th>
        <th>Description</th>
        <th>Enabled</th>
        <th>Actions</th>
      </tr>
    </thead>
  )

  _getItems = () => this.props.languages

  _getItemElementPrototype = () => Item

  _getItemProps = (itemId) => ({
    toggleEnable: (isEnable) => this.props.toggleEnable(itemId, isEnable)
  })

  _shouldAddItemRender = () => {
    const { children } = this.props
    return children && children.type === AddItemContainer
  }

  _shouldDeleteItemRender = (itemId) => {
    const { routeId, children } = this.props
    return routeId == itemId && children && children.type === DeleteItemContainer
  }

}

List.propTypes = {
  languages: PropTypes.objectOf(LanguagePropType),
  toggleEnable: PropTypes.func.isRequired,
  routeId: PropTypes.string
}

export default List
