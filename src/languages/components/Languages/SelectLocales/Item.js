import React, { PropTypes } from 'react'

const Item = ({
  locale,
  description
}) => {
  const countryCode = locale.slice(-2).toLowerCase()

  return (
    <div className="item" data-value={locale}>
      <i className={`${countryCode} flag`}></i>{description}
    </div>
  )
}

Item.propTypes = {
  locale: PropTypes.string.isRequired,
  description: PropTypes.string.isRequired
}

export default Item
