import React, { PropTypes } from 'react'
import jq from 'jquery'
import classNames from 'classnames'
import transition from 'semantic-ui-transition'
import uiDropdown from 'semantic-ui-dropdown'
import Item from './Item'
import api from 'languages/api'

class SelectLocales extends React.Component {

  componentDidMount() {
    if (jq.fn.transition === undefined) jq.fn.transition = transition
    jq.fn.dropdown = uiDropdown

    const { onChangeHandler } = this.props

    jq(this._dropdown).dropdown({
      onChange: (value, text, $selectedItem) => {
        const selectedItem = $selectedItem[0]        
        onChangeHandler(
          selectedItem.getAttribute('data-value'),
          selectedItem.textContent
        )
      }
    })
  }

  render() {
    const { isLoading, locales } = this.props
    const dropdownClass = classNames('ui fluid search selection dropdown', {'loading': isLoading})

    return (
      <div className={dropdownClass} ref={(c) => this._dropdown = c}>
        <input type="hidden" name="country"/>

        <i className="dropdown icon"></i>
        <div className="default text">Select Country</div>
        <div className="menu">
          {Object.keys(locales).map((locale, idx) => (
            <Item
              key={idx}
              locale={locale}
              description={locales[locale]}/>
          ))}
        </div>
    </div>
    )
  }
}

SelectLocales.propTypes = {
  isLoading: PropTypes.bool.isRequired,
  locales: PropTypes.object.isRequired,
  onChangeHandler: PropTypes.func.isRequired
}

export default SelectLocales
