import React, { PropTypes } from 'react'
import { Link } from 'react-router'
import config from 'languages/config.json'

const ActionsMenu = () => {
  return (
    <div className="ui top attached inverted grey menu borderless">
      <Link
        to={`${config.routes.languages.index}/${config.routes.languages.add}`}
        className="item">
        <i className="large icons">
          <i className="inverted grey world icon"></i>
          <i className="inverted grey corner add icon"></i>
        </i>
        New language
      </Link>
    </div>
  )
}

export default ActionsMenu
