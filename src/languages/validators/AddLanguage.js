import Validator from 'common/Validator'

class AddLanguage extends Validator {

  locale(value, languagesLocales) {
    if (!value) {
      return this._errors['locale'] = 'Locale is required'
    }

    for (const key in languagesLocales)
      if (languagesLocales.hasOwnProperty(key)) {
        if (languagesLocales[key].locale === value) {
          return this._errors['locale'] = 'Locale is already added'
        }
      }

    return true
  }
}

export default new AddLanguage()
