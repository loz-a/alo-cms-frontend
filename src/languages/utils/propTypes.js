import { PropTypes } from 'react'

export const LocalePropType = PropTypes.shape({
  id: PropTypes.number.isRequired,
  value: PropTypes.string.isRequired
})

export const DisplayValuePropType = PropTypes.shape({
  id: PropTypes.number.isRequired,
  value: PropTypes.string.isRequired,
  locale: LocalePropType.isRequired
})

export const LanguagePropType = PropTypes.shape({
  id: PropTypes.number.isRequired,
  language: PropTypes.string.isRequired,
  locale: LocalePropType.isRequired,
  description: PropTypes.string,
  isEnable: PropTypes.bool.isRequired
})
