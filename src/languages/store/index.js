import { createStore, applyMiddleware, compose } from 'redux'
import reducers from '../reducers'
import thunk from 'redux-thunk'
import createLogger from 'redux-logger'
import api from 'common/middlewares/api'

const middlewares = [thunk, api]

if (DEVELOPMENT) {
  middlewares.push(createLogger())
}

const enhancer = compose(
  applyMiddleware(...middlewares),
  window.devToolsExtension ? window.devToolsExtension() : (f) => f
)

const store = createStore(reducers, {}, enhancer)

if (DEVELOPMENT) {
  window.store = store
}

export default store
