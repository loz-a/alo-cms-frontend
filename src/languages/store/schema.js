import { Schema, arrayOf, valuesOf } from 'normalizr'

const toInt = (...convertAttributes) =>
  (output, key, value, input) => {
    if (convertAttributes.indexOf(key) >= 0) output[key] = parseInt(output[key])
    else output[key] = value
    return output
  }

const replaceKeys = (keysPairs) =>
  (output, key, value, input) => {
    if (key in keysPairs) {
      output[keysPairs[key]] = output[key]
      delete output[key]
    }
    return output
  }


const locale = new Schema('locales', { assignEntity: toInt('id') })

const language = new Schema('languages', {
  assignEntity: (output, key, value, input) => {
    toInt('id', 'locale')(output, key, value, input)

    replaceKeys({
      display_values: 'displayValues',
      is_enable: 'isEnable'
    })(output, key, value, input)

    if (key === 'is_enable') {
      output['isEnable'] = value === '1' ? true : false
    }
  }
})

const displayValue = new Schema('display_values', { assignEntity: toInt('id', 'locale') })

language.define({
  locale: locale
})

language.define({
  display_values: valuesOf(displayValue)
})

displayValue.define({
  locale: locale
})

export default {
  languages: arrayOf(language)
}
