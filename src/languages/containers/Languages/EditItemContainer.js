import React from 'react'
import { connect } from 'react-redux'
import EditItem from 'languages/components/Languages/Item/EditItem'
import { getLanguageById } from 'languages/reducers'
import { editLanguage } from 'languages/actions'

const EditItemContainer = ({
  language,
  editLanguage,
  children
}) => {  
  return (
    <EditItem language={language} editLanguage={editLanguage}>
      {children}
    </EditItem>
  )
}

export default connect (
  (state, props) => ({
    language: getLanguageById(state, props.params.id)
  }),
  {
    editLanguage
  }
)(EditItemContainer)
