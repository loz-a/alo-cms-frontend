import React from 'react'
import { connect } from 'react-redux'
import List from 'languages/components/Languages/List'
import { fetchLanguages, toggleEnable } from 'languages/actions'
import * as reducers from 'languages/reducers'


class ListContainer extends React.Component {

  componentDidMount() {
    const { fetchLanguages, isLoading, isLoaded } = this.props
    if (!isLoading && !isLoaded) fetchLanguages()
  }

  render() {
    const {
      isLoading,
      languages,
      toggleEnable,
      languagesErrorMessage,
      supportedLocalesErrorMessage,
      params
    } = this.props

    return (
      <div>

        { (languagesErrorMessage || supportedLocalesErrorMessage) &&
          <div className="ui icon negative message">
            <i className="warning circle icon"></i>
            <div className="content">
              <div className="header">An error occurred</div>
              { languagesErrorMessage && <p>{languagesErrorMessage}</p> }
              { supportedLocalesErrorMessage && <p>{supportedLocalesErrorMessage}</p> }
            </div>
          </div>
        }

        { isLoading &&
          <div className="ui active inverted dimmer">
            <div className="ui text loader">Loading</div>
          </div>
        }
        
        <List
          toggleEnable={toggleEnable}
          languages={languages}
          routeId={params.id || null}>
          {this.props.children}
        </List>
      </div>
    )
  }
}

export default connect(
  (state) => ({
    isLoading: reducers.isLanguagesLoading(state),
    isLoaded: reducers.isLanguagesLoaded(state),
    languages: reducers.getLanguages(state),
    languagesErrorMessage: reducers.getLanguagesErrorMessage(state),
    supportedLocalesErrorMessage: reducers.getSupportedLocalesErrorMessage(state)
  }),
  {
    fetchLanguages,
    toggleEnable
  }
)(ListContainer)
