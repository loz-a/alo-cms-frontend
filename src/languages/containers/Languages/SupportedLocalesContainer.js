import React, { PropTypes } from 'react'
import { connect } from 'react-redux'
import SelectLocales from 'languages/components/Languages/SelectLocales'
import { fetchSupportedLocales } from 'languages/actions'
import { getSupportedLocales, isSupportedLocalesLoading, isSupportedLocalesLoaded } from 'languages/reducers'

class SupportedLocalesContainer extends React.Component {

  componentDidMount() {
    const { fetchSupportedLocales, isLoaded, isLoading } = this.props
    if (!isLoaded && !isLoading) fetchSupportedLocales()
  }

  render() {
    const { isLoading, locales, onChangeHandler, filterLocales } = this.props

    return (
      <SelectLocales
        locales={ (filterLocales) ? filterLocales(locales) : locales }
        isLoading={isLoading}
        onChangeHandler={onChangeHandler} />
    )
  }
}

SupportedLocalesContainer.propTypes = {
  onChangeHandler: PropTypes.func.isRequired,
  filterLocales: PropTypes.func
}

export default connect(
  (state) => ({
    locales: getSupportedLocales(state),
    isLoading: isSupportedLocalesLoading(state),
    isLoaded: isSupportedLocalesLoaded(state)
  }),
  {
    fetchSupportedLocales
  }
)(SupportedLocalesContainer)
