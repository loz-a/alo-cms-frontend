import React from 'react'
import { connect } from 'react-redux'
import AddItem from 'languages/components/Languages/Item/AddItem'
import { addLanguage, clearValidateErrors } from 'languages/actions'
import { getLanguagesLocales, getLanguagesValidateErrors } from 'languages/reducers'

const AddItemContainer = ({
  addLanguage,
  clearValidateErrors,
  locales,
  validateErrors
}) => (
  <AddItem
    addLanguage={addLanguage}
    clearValidateErrors={clearValidateErrors}
    locales={locales}
    validateErrors={validateErrors}/>
)

export default connect(
  (state) => ({
      'locales': getLanguagesLocales(state),
      'validateErrors': getLanguagesValidateErrors(state)
  }),
  {
    addLanguage,
    clearValidateErrors
  }
)(AddItemContainer)
