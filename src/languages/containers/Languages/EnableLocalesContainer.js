import React, { PropTypes } from 'react'
import { connect } from 'react-redux'
import { getLanguagesLocales } from 'languages/reducers'
import { LocalePropType } from 'languages/utils/propTypes'
import SupportedLocalesContainer from './SupportedLocalesContainer'

const EnableLocalesContainer = ({
  enableLocales,
  ...otherProps
}) => {

  const filterLocales = (supportedLocales) => {

    if (!Object.keys(supportedLocales).length) return supportedLocales

    return Object.keys(enableLocales)
      .reduce((acc, id) => {
        const enableLocale = enableLocales[id].value
        acc[enableLocale] = supportedLocales[enableLocale]        
        return acc
      }, {})
  }

  return <SupportedLocalesContainer {...otherProps } filterLocales={filterLocales} />
}

EnableLocalesContainer.propTypes = {
  enableLocales: PropTypes.objectOf(LocalePropType)
}

export default connect (
  (state) => ({
    enableLocales: getLanguagesLocales(state)
  })
)(EnableLocalesContainer)
