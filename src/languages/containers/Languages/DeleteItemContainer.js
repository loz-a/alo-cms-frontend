import React from 'react'
import { connect } from 'react-redux'
import DeleteItem from 'languages/components/Languages/Item/DeleteItem'
import * as reducers from 'languages/reducers'
import { deleteLanguage } from 'languages/actions'

const DeleteItemContainer = ({
  language,
  deleteLanguage
}) => (
  <DeleteItem language={language} deleteLanguage={deleteLanguage}/>
)


export default connect(
  (state, props) => ({
    language: reducers.getLanguageById(state, props.params.id)
  }),
  {
    deleteLanguage
  },
  null
)(DeleteItemContainer)
