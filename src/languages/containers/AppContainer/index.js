import React, { PropTypes } from 'react'
import { Provider } from 'react-redux'
import store from 'languages/store'

import ModuleMenu from 'languages/components/ModuleMenu'
import LanguagesModuleHeader from 'languages/components/LanguagesModuleHeader'
import ModulesNavigation from 'common/components/ModulesNavigation'
import AppConfigProvider from '../AppConfigProvider'
import withTranslate from 'app-config/decorators/withTranslate'
import './styles.css'

import { initToken } from 'languages/actions'
import { sessionGet } from 'common/utils'


class AppContainer extends React.Component {

  componentDidMount() {
    store.dispatch(initToken(sessionGet('token')))
  }

  render() {
    const { children } = this.props

    return (
      <Provider store={store}>
        <AppConfigProvider>
          <ModulesNavigation>
            <ModuleMenu />
            <LanguagesModuleHeader />
            {children}
          </ModulesNavigation>
        </AppConfigProvider>
      </Provider>
    )
  }
}

export default AppContainer
