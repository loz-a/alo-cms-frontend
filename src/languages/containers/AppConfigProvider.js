import AppConfigProviderFactory from 'app-config/containers/AppConfigProviderFactory'
import { getAppConfig, isAppConfigLoading, isAppConfigErrorLoaded } from 'languages/reducers'
import config from 'languages/config.json'

export default AppConfigProviderFactory({
  getAppConfig,
  isAppConfigLoading,
  isAppConfigErrorLoaded,
  configUrl: config.url.config
})
