import React from 'react'
import { connect } from 'react-redux'
import { signIn } from 'auth/actions/signIn'
import { isAuthenticating, getAuthenticateError } from 'auth/reducers'
import SignInForm from 'auth/components/SignInForm'

const SignInContainer = ({
  isAuthenticating,
  error,
  signIn
}) => {
  return (
    <SignInForm signIn={signIn}
      isAuthenticating={isAuthenticating}
      error={error}/>
  )
}

export default connect(
  (state, ownProps) => ({
    isAuthenticating: isAuthenticating(state),
    error: getAuthenticateError(state)
  }),
  {
    signIn
  }
)(SignInContainer)
