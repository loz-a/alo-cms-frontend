import React from 'react'
import { Provider } from 'react-redux'
import store from 'auth/store'
import AppConfigProvider from '../AppConfigProvider'
import './styles.css'

const AppContainer = ({ children }) => {
  
  return (
    <Provider store={store}>
      <AppConfigProvider>
        {children}
      </AppConfigProvider>
    </Provider>
  )
}

export default AppContainer
