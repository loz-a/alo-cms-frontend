import AppConfigProviderFactory from 'app-config/containers/AppConfigProviderFactory'
import { getAppConfig, isAppConfigLoading, isAppConfigErrorLoaded } from 'auth/reducers'
import config from 'auth/config.json'

export default AppConfigProviderFactory({
  getAppConfig,
  isAppConfigLoading,
  isAppConfigErrorLoaded,
  configUrl: config.configUrl
})
