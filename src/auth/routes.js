import { Router, Route, IndexRoute } from 'react-router'
import React from 'react'
import history from 'common/history'

import PageNotFound from 'common/components/PageNotFound'
import AppContainer from './containers/AppContainer'
import SignInContainer from './containers/SignIn'
import config from './config.json'

export default (
  <Router history={history}>
    <Route path={config.routes.login} component={AppContainer}>
      <IndexRoute component={SignInContainer} />
    </Route>
    <Route path="*" component={PageNotFound} />
  </Router>
)
