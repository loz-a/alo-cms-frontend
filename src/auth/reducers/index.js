import { compose, combineReducers } from 'redux'
import auth, * as fromAuth from './auth'
import appConfig, * as fromAppConfig from 'app-config/reducers'

export default combineReducers({
  auth,
  appConfig
})

export const isAppConfigErrorLoaded = (state) => fromAppConfig.isErrorLoaded(state)

export const isAppConfigLoading = (state) => fromAppConfig.isLoading(state)

export const getAppConfig = (state) => fromAppConfig.getData(state)

export const getLogin = (state) => fromAuth.getLogin(state)

export const getToken = (state) => fromAuth.getToken(state)

export const isAuthenticating = (state) => fromAuth.isAuthenticating(state)

export const getAuthenticateError = (state) => fromAuth.getError(state)
