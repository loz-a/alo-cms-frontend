import * as constants from '../constants'
import jwtDecode from 'jwt-decode'

const INIT_STATE = {
  login: null,
  token: null,
  isAuthenticating: false,
  error: null
}

export default function(state = INIT_STATE, action) {

  switch (action.type) {
    case constants.SIGN_IN_REQUEST:
      return {
        ...state,
        isAuthenticating: true,
        error: null
      }

    case constants.SIGN_IN_SUCCESS:
      const { token } = action.payload
      if (token) return {
        login: jwtDecode(token).data.login,
        token: token,
        isAuthenticating: false,
        error: null
      }

    case constants.SIGN_IN_FAILURE:
      const {error} = action.error
      return {
        ...state,
        isAuthenticating: false,
        error: error
      }
    case constants.SIGN_OUT:
      return INIT_STATE

    default:
      return state
  }
}

export const getLogin = (state) => state.auth.login

export const getToken = (state) => state.auth.token

export const isAuthenticating = (state) => state.auth.isAuthenticating

export const getError = (state) => state.auth.error
