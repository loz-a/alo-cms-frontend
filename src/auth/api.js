/**
 * Mocking client-server processing
 */
 // import data from './mock.json'
 //
 // const TIMEOUT = 1200
 //
 // export default {
 //   signIn(signInData, timeout = TIMEOUT) {
 //      // @todo mock reject
 //      return new Promise(
 //         (resolve, reject) => setTimeout(() => {
 //            resolve(data)
 //            // reject({
 //            //    error: 'something was wrong'
 //            // })
 //         }, timeout)
 //      )
 //   }
 // }

import { httpClient } from 'common/utils'
import config from 'auth/config.json'

export default {
   signIn(signInData) {
      return httpClient().post(config.api.login, signInData)
   },
   signOut() {
     return httpClient().get(config.api.logout)
   }
}
