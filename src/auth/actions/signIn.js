import jwtDecode from 'jwt-decode'
import api from  '../api'
import * as constants from '../constants'
import { checkHttpStatus, sessionSet, sessionRemove, redirect } from 'common/utils'

const signInRequest = () => ({
  type: constants.SIGN_IN_REQUEST
})

const signInSuccess = (token) => ({
  type: constants.SIGN_IN_SUCCESS,
  payload: { token }
})

const signInFailure = (error) => ({
  type: constants.SIGN_IN_FAILURE,
  error
})

export const signIn = (data) =>
  (dispatch) => {
    dispatch(signInRequest())

    return api
      .signIn(data)
      .then(checkHttpStatus)
      .then(
        (response) => {
          try {
            const { token, redirectRoute } = response.data
            jwtDecode(token)
            dispatch(signInSuccess(token))
            sessionSet('token', token)
            redirect(redirectRoute)
          } catch (e) {
            sessionRemove('token')
            dispatch(signInFailure({
              error: 'Authorization error. Please check login or/and password'
            }))
          }
        },
        (error) => {
          sessionRemove('token')
          dispatch(signInFailure(error.response.data))
        }
      )
  }
