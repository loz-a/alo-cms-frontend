import { SIGN_OUT_REQUEST } from 'auth/constants'
import api from 'auth/api'
import { sessionRemove, redirect } from 'common/utils'
import config from 'auth/config.json'

const signOutRequest = () => ({
  type: SIGN_OUT_REQUEST
})

export const signOut = () =>
  (dispatch) => {
    dispatch(signOutRequest())
    sessionRemove('token')

    return api
      .signOut()
      .then(
        () => redirect(config.routes.login),
        () => redirect(config.routes.login)
      )
  }
