import React, { PropTypes } from 'react'
import cx from 'classnames'
import withTranslate from 'app-config/decorators/withTranslate'

class SignInForm extends React.Component {

  state = {
    login: '',
    password: ''
  }

  onSubmit = (evt) => {
    evt.preventDefault()
    this.props.signIn(this.state)
  }

  handleChange = (input) => (evt) => {
    this.setState({
      [input]: evt.target.value
    })
  }

  render() {
    const { login, password } = this.state
    const { isAuthenticating, error, translate } = this.props
    const classNames = cx('ui large form', {loading: isAuthenticating})

    return (
      <div className="ui middle aligned center aligned grid">
        <div className="column">

          <form onSubmit={this.onSubmit} className={classNames}>

            <div className="ui attached message">
              <div className="header">
                  <i className="sign in icon"></i> {translate('Please sign in')}
              </div>
            </div>

            <div className="ui attached fluid segment">

              <div className="field">
                  <div className="ui left icon input">
                      <i className="user icon"></i>
                      <input
                        name="login"
                        placeholder="Login"
                        autoFocus
                        onChange={this.handleChange('login')}
                        value={login}/>
                  </div>
              </div>

              <div className="field">
                  <div className="ui left icon input">
                      <i className="lock icon"></i>
                      <input
                        name="password"
                        type="password"
                        placeholder="password"
                        onChange={this.handleChange('password')}
                        value={password}/>
                  </div>
              </div>

              <input
                type="submit"
                className="ui fluid large teal submit button"
                defaultValue={translate('Sign in')}/>

            </div>

            {error &&
              <div className="ui bottom attached negative message">
                  <i className="warning sign help"></i> {error}
              </div>
            }

          </form>
        </div>
      </div>
    )
  }
}

SignInForm.propTypes = {
  signIn: PropTypes.func.isRequired,
  isAuthenticating: PropTypes.bool.isRequired,
  error: PropTypes.string
}

export default withTranslate(SignInForm)
