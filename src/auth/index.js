import React from 'react'
import { render } from 'react-dom'
import routes from './routes'

localStorage.debug = null

render(
  routes,
  document.getElementById('app')
)
