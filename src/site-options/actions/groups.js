import { normalize } from 'normalizr'
import * as schema from './schema'
import api from '../api'
import { isGroupsLoaded } from '../reducers'
import {
  GROUPS_REQUEST,
  GROUPS_RECEIVE,
  GROUPS_RECEIVE_FAILURE,
  GROUP_UPDATE_START,
  GROUP_UPDATE_SUCCESS,
  GROUP_UPDATE_FAILURE
} from '../constancts'

const requestGroups = () => ({
  type: GROUPS_REQUEST
})

const receiveGroups = (response) => {
  const isValidResult = response.length && response.length > 0
  return {
    type: GROUPS_RECEIVE,
    response: isValidResult ? normalize(response, schema.arrayOfGroups) : null
  }
}

const receiveGroupsFailure = (error) => ({
  type: GROUPS_RECEIVE_FAILURE,
  error
})

export const fetchGroups = () =>
  (dispatch, getState) => {
    if (isGroupsLoaded(getState())) return Promise.resolve()

    dispatch(requestGroups())

    return api
      .fetchGroups()
      .then(
        (response) => dispatch(receiveGroups(response)),
        (error)    => dispatch(receiveGroupsFailure(error))
      )
  }
