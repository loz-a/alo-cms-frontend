import { Schema, arrayOf } from 'normalizr'

export const group   = new Schema('groups')
export const options = new Schema('options')

group.define({
  options: arrayOf(options)
})

export const arrayOfGroups = arrayOf(group)
