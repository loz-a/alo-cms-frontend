import api from '../api'
import {
  OPTIONS_UPDATE_START,
  OPTIONS_UPDATE_SUCCESS,
  OPTIONS_UPDATE_FAILURE
} from '../constancts'

const updateStart = () => ({
  type: OPTIONS_UPDATE_START
})

const updateSuccess = (response) => ({
  type: OPTIONS_UPDATE_SUCCESS,
  response
})

const updateFailure = (error) => ({
  type: OPTIONS_UPDATE_FAILURE,
  error
})

export const updateOptions = (data) =>
  (dispatch) => {
    dispatch(updateStart(data))

    return api
      .updateOptions(data)
      .then(
        (response) => dispatch(updateSuccess(response)),
        (error)    => dispatch(updateFailure(error))
      )
  }
