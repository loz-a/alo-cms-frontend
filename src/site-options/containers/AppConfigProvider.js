// import React from 'react'
// import { connect } from 'react-redux'
// import AppConfigProvider from 'common/components/AppConfigProvider'
// import { fetchConfig } from 'common/actions/appConfig'
import AppConfigProviderFactory from 'app-config/containers/AppConfigProviderFactory'
import { getAppConfig, isAppConfigLoading, isAppConfigErrorLoaded } from '../reducers'

// export default connect(
//   (state) => ({
//     appConfig: getAppConfig(state),
//     isLoading: isAppConfigLoading(state),
//     isErrorLoaded: isAppConfigErrorLoaded(state)
//   }),
//   {
//     fetchConfig
//   }
// )(AppConfigProvider)

export default AppConfigProviderFactory({
  getAppConfig,
  isAppConfigLoading,
  isAppConfigErrorLoaded
})
