import React, { PropTypes } from 'react'
import { Provider } from 'react-redux'
import store from 'site-options/store'

import ModuleMenu from 'site-options/components/ModuleMenu'
import ModuleHeader from 'common/components/ModuleHeader'
import ModulesNavigation from 'common/components/ModulesNavigation'
import AppConfigProvider from 'site-options/containers/AppConfigProvider'
import { Divider } from 'stardust'
import './styles.css'


class AppContainer extends React.Component {

  render() {
    const { children } = this.props

    return (
      <Provider store={store}>
        <AppConfigProvider>
          <ModulesNavigation>
            <ModuleMenu />

            <ModuleHeader
              headerText="Site Options"
              subheaderText="Manage your data which will be used as common site options" />

            <Divider />
            {children}
          </ModulesNavigation>
        </AppConfigProvider>
      </Provider>
    )
  }
}

export default AppContainer
