import React, { PropTypes } from 'react'
import { connect } from 'react-redux'
import OptionsList from 'site-options/components/Options/List'
import { GroupPropType } from 'site-options/utils/propTypes'
import { getOptionsByGroup, isOptionsItemLoading } from 'site-options/reducers'
import { updateOptions } from 'site-options/actions/options'

class OptionsContainer extends React.Component {
  render() {
    const { options, activeGroup, updateOptions, isItemLoading } = this.props

    return (
      <div>
        <OptionsList
          options={options}
          activeGroup={activeGroup}
          updateOptions={updateOptions}
          isItemLoading={isItemLoading} />
      </div>
    )
  }
}

OptionsContainer.propTypes = {
  activeGroup: GroupPropType
}

export default connect(
  (state, ownProps) => ({
    options: getOptionsByGroup(state, ownProps.activeGroup),
    isItemLoading: isOptionsItemLoading(state)
  }),
  {
    updateOptions
  }
)(OptionsContainer)
