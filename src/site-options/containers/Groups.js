import React from 'react'
import { connect } from 'react-redux'
import { fetchGroups } from '../actions/groups'
import { getGroups, isGroupsLoading, isGroupsErrorLoading } from '../reducers'
import Groups from '../components/Groups'

class GroupsContainer extends React.Component {
  componentDidMount() {
    this.props.fetchGroups()
  }

  render() {
    const { groups, isLoading, isErrorLoading } = this.props

    return (
      <Groups
        groups={groups}
        activeGroup={this.getActiveGroup()}
        isLoading={isLoading}
        isErrorLoading={isErrorLoading} />
    )
  }

  getActiveGroup = () => {
    const { groups, params } = this.props
    const groupId = parseInt(params.id)
    return groups[groupId] ? groups[groupId] : null
  }

}
export default connect(
  (state) => ({
    groups: getGroups(state),
    isLoading: isGroupsLoading(state),
    isErrorLoading: isGroupsErrorLoading(state)
  }),
  {
    fetchGroups
  }
)(GroupsContainer)
