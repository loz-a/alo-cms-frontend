import { PropTypes } from 'react'

export const GroupPropType = PropTypes.shape({
  id: PropTypes.number.isRequired,
  name: PropTypes.string.isRequired,
  info: PropTypes.arrayOf(PropTypes.number.isRequired)
})

export const OptionsPropType = PropTypes.shape({
  id: PropTypes.number.isRequired,
  name: PropTypes.string.isRequired,
  value: PropTypes.string.isRequired,
  description: PropTypes.string
})
