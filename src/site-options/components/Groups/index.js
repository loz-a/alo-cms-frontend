import React, { PropTypes } from 'react'
import { Link } from 'react-router'
import { Message, Header } from 'stardust'
import { GroupPropType } from 'site-options/utils/propTypes'
import OptionsList from 'site-options/containers/Options'
import './styles.css'

class Groups extends React.Component {
  render() {
    const { groups, activeGroup, isLoading, isErrorLoading } = this.props

    if (isErrorLoading) return (
      <Message className="negative" icon="warning circle">
        <Header>
          An error occurred while loading data
        </Header>
       <p>Тry again or contact the developer</p>
     </Message>
    )

    return (
      <div className="groups-box">
        <div className="ui grid">
          <div className="four wide column">
            <div className="ui vertical fluid tabular menu">

              {Object.keys(groups).map((id) => (
                <Link
                  to={`/site-options/group/${id}`}
                  className="item"
                  activeClassName="active"
                  key={id}
                >
                  {groups[id].name}
                </Link>
              ))}

            </div>
          </div>
          <div className="twelve wide stretched column">

            {activeGroup && <OptionsList activeGroup={activeGroup} />}

            {!activeGroup && !isLoading &&
              <div className="label-box">
                <div className="ui left pointing label">Please select some group!</div>
              </div>
            }

          </div>
        </div>

        {isLoading &&
          <div className="ui active inverted dimmer">
            <div className="ui text loader">Loading</div>
          </div>
        }

      </div>
    )
  }
}

Groups.propTypes = {
  groups: PropTypes.objectOf(GroupPropType),
  activeGroup: GroupPropType,
  isLoading: PropTypes.bool.isRequired,
  isErrorLoading: PropTypes.bool.isRequired
}

export default Groups
