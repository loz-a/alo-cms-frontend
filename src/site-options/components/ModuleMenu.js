import React, { PropTypes } from 'react'
import Menu from 'common/components/semantic-ui/collections/Menu/Menu'
import RightSubmenu from 'common/components/ModuleMenu/RightSubmenu'
import withAppConfig from 'app-config/decorators/withAppConfig'

const ModuleMenu = ({
  appConfig
}) => (
  <Menu className="top attached">
    <RightSubmenu appConfig={appConfig} />
  </Menu>
)

ModuleMenu.propTypes = {
  appConfig: PropTypes.object.isRequired
}

export default withAppConfig(ModuleMenu)
