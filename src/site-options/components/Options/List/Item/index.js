import React, { PropTypes } from 'react'
import { Button, Icon } from 'stardust'
import { OptionsPropType } from 'site-options/utils/propTypes'
import Form from './Form'
import './styles.css'

const Item = ({
  isEdit,
  toggleEdit,
  optionsItem,
  updateOptions
}) => {
  if (isEdit) return (
    <Form
      optionsItem={optionsItem}
      toggleEdit={toggleEdit}
      updateOptions={updateOptions} />
  )

  const { name, value, description } = optionsItem
  return (
    <div className="item">

        <div className="right floated content">
          <Button className="compact basic icon mini" title="edit" onClick={toggleEdit}>
            <Icon name="write" size="large" link />
          </Button>
        </div>

        <div className="content">
          <div className="header">
            {name}: <span className="value">{value}</span>
          </div>
          <div className="description">{description}</div>
        </div>

    </div>
  )
}


Item.propTypes = {
  optionsItem: OptionsPropType.isRequired,
  isEdit: PropTypes.bool.isRequired,
  toggleEdit: PropTypes.func.isRequired,
  updateOptions: PropTypes.func.isRequired
}

export default Item
