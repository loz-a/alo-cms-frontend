import React, { PropTypes } from 'react'
import { Button, Icon, Input } from 'stardust'
import { OptionsPropType } from 'site-options/utils/propTypes'

class Form extends React.Component {

  state = {
    id: undefined,
    name: '',
    value: '',
    description: ''
  }

  componentDidMount() {
    const { optionsItem } = this.props
    this.setState({ ...optionsItem })
  }

  onSubmit = (evt) => {
    const { toggleEdit, updateOptions } = this.props

    // @TODO validations and loading
    updateOptions(this.state)
    toggleEdit()

    evt.preventDefault()
  }

  handleChange = (input) => (evt) => {
    this.setState({
      [input]: evt.target.value
    })
  }

  render() {
    const { name, value, description } = this.state

    return (
      <form className="item" onSubmit={this.onSubmit}>

        <div className="right floated content">
          <Button className="compact icon green mini" title="save" type="submit">
            <Icon name="checkmark" size="large" link />
          </Button>
        </div>

        <div className="content">
          <div className="header">
            <div className="def-term">{name}:</div>
            <Input
              className="transparent"
              icon="write"
              placeholder={value}
              onChange={this.handleChange('value')} />
          </div>

          <div className="description">
            <div className="def-descr">description:</div>
            <Input
              className="transparent"
              icon="write"
              placeholder={description}
              onChange={this.handleChange('description')}/>
          </div>
        </div>

      </form>
    )
  }
}

Form.propTypes = {
  optionsItem: OptionsPropType.isRequired,
  toggleEdit: PropTypes.func.isRequired,
  updateOptions: PropTypes.func.isRequired
}

export default Form
