import React, { PropTypes } from 'react'
import { List } from 'stardust'
import { OptionsPropType, GroupPropType } from 'site-options/utils/propTypes'
import ListItem from './Item'
import OneOpen from 'common/decorators/OneActive'

class OptionsList extends React.Component {

  componentWillReceiveProps(nextProps) {
    const { activeGroup, resetActive } = this.props
    if (!Object.is(activeGroup, nextProps.activeGroup)) resetActive()
  }

  getItems = () => {
    const { options, isActive, toggleActive, updateOptions } = this.props
    if (!options) return null

    return options.map((item) => (
      <ListItem
        key={item.id}
        optionsItem={item}
        isEdit={isActive(item.id)}
        toggleEdit={toggleActive(item.id)}
        updateOptions={updateOptions} />
    ))
  }

  render() {
    const { isItemLoading } = this.props

    return (
      <List className="large divided selection">
        {this.getItems()}

        { isItemLoading &&
          <div className="ui active inverted dimmer">
            <div className="ui mini text loader">Loading</div>
          </div>
        }

      </List>
    )
  }
}

OptionsList.propTypes = {
  options: PropTypes.arrayOf(OptionsPropType),
  activeGroup: GroupPropType,
  isActive: PropTypes.func.isRequired,
  toggleActive: PropTypes.func.isRequired,
  resetActive: PropTypes.func.isRequired,
  updateOptions: PropTypes.func.isRequired,
  isItemLoading: PropTypes.bool.isRequired
}

export default OneOpen(OptionsList)
