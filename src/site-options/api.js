/**
 * Mocking client-server processing
 */
 import data from './mock.json'

 const TIMEOUT = 1200

 export default {
   fetchGroups(timeout = TIMEOUT) {
      // @todo mock reject
      return new Promise(
         (resolve, reject) => setTimeout(() => {
            resolve(data)
            // reject({
            //    error: 'something was wrong'
            // })
         }, timeout)
      )
   },

   updateOptions(data, timeout = TIMEOUT) {
      // @todo mock reject
      return new Promise(
         (resolve, reject) => setTimeout(() => {
            resolve(data)
            // reject({
            //    error: 'something was wrong'
            // })
         }, timeout)
      )
   }
 }
