import { combineReducers } from 'redux'
import groups, * as fromGroups from './groups'
import options, * as fromOptions from './options'
import appConfig, * as fromAppConfig from 'app-config/reducers'

export default combineReducers({
  groups,
  options,
  appConfig
})

export const getGroups = (state) => fromGroups.getGroups(state)

export const isGroupsLoading = (state) => fromGroups.isLoading(state)

export const isGroupsLoaded = (state) => fromGroups.isLoaded(state)

export const isGroupsErrorLoading = (state) => fromGroups.isErrorLoading(state)

export const getOptions = (state) => fromOptions.getOptions(state)

export const getOptionsByGroup = (state, group) => fromOptions.getOptionsByGroup(state, group)

export const isOptionsItemLoading = (state) => fromOptions.isItemLoading(state)

export const isAppConfigErrorLoaded = (state) => fromAppConfig.isErrorLoaded(state)

export const isAppConfigLoading = (state) => fromAppConfig.isLoading(state)

export const getAppConfig = (state) => fromAppConfig.getData(state)
