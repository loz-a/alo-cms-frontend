import { combineReducers } from 'redux'
import {
  GROUPS_REQUEST,
  GROUPS_RECEIVE,
  GROUPS_RECEIVE_FAILURE,
  OPTIONS_UPDATE_START,
  OPTIONS_UPDATE_SUCCESS,
  OPTIONS_UPDATE_FAILURE
} from '../constancts'


const init = {
  byId: {},
  loading: false,
  loaded: false,
  errorLoading: false
}


function byId(state = init.byId, action) {
  switch (action.type) {
    case GROUPS_RECEIVE:
      if (action.response) {
        return {
          ...state,
          ...action.response.entities.groups
        }
      }
    default:
      return state
  }
}

function loading(state = init.loading, action) {
  switch (action.type) {
    case GROUPS_REQUEST:
      return true
    case GROUPS_RECEIVE:
    case GROUPS_RECEIVE_FAILURE:
      return false
    default:
      return state
  }
}

function loaded(state = init.loaded, action) {
  switch (action.type) {
    case GROUPS_RECEIVE:
      return true
    case GROUPS_RECEIVE_FAILURE:    
      return false
    default:
      return state
  }
}

function errorLoading(state = init.errorLoading, action) {
  switch (action.type) {
    case GROUPS_REQUEST:
    case GROUPS_RECEIVE:
    case OPTIONS_UPDATE_SUCCESS:
    case OPTIONS_UPDATE_START:
      return false
    case GROUPS_RECEIVE_FAILURE:
    case OPTIONS_UPDATE_FAILURE:
      return true
    default:
      return state
  }
}

const load = combineReducers({
  loading,
  loaded,
  errorLoading
})

export default combineReducers({
  byId,
  load
})

export const isErrorLoading = (state) => state.groups.load.errorLoading

export const isLoaded = (state) => state.groups.load.loaded

export const isLoading = (state) => state.groups.load.loading

export const getGroups = (state) => state.groups.byId
