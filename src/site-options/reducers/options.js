import { combineReducers } from 'redux'
import {
  GROUPS_RECEIVE,
  OPTIONS_UPDATE_START,
  OPTIONS_UPDATE_SUCCESS,
  OPTIONS_UPDATE_FAILURE
} from '../constancts'

const init = {
  byId: {},
  itemLoading: false
}

function byId(state = init.byId, action) {
  switch (action.type) {
    case GROUPS_RECEIVE:
      if (action.response) {
        return {
          ...state,
          ...action.response.entities.options
        }
      }
    case OPTIONS_UPDATE_SUCCESS:
      const { response } = action
      return {
        ...state,
        [response.id]: response
      }
    case OPTIONS_UPDATE_FAILURE:
    default:
      return state
  }
}

function itemLoading(state = init.itemLoading, action) {
  switch (action.type) {
    case OPTIONS_UPDATE_START:
      return true
    case OPTIONS_UPDATE_SUCCESS:
    case OPTIONS_UPDATE_FAILURE:
      return false
    default:
      return state
  }
}

export default combineReducers({
  byId,
  itemLoading
})

export const getOptions = (state) => state.options.byId

export const getOptionsByGroup = (state, group) => {
  if (!group || !group.options) return null
  return group.options.reduce((arr, optionsId) => {
    arr.push(state.options.byId[optionsId])
    return arr
  }, [])
}

export const isItemLoading = (state) => state.options.itemLoading
