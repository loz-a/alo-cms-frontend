import { Router, Route, IndexRoute } from 'react-router'
import React from 'react'
import history from 'common/history'

import PageNotFound from 'common/components/PageNotFound'
import AppContainer from './containers/AppContainer'
import Groups from './containers/Groups'
import config from './config.json'

export default (
  <Router history={history}>
    <Route path={config.routes.siteOptions} component={AppContainer}>
      <IndexRoute component={Groups} />
      <Route path={config.routes.group} component={Groups} />
    </Route>
    <Route path="*" component={PageNotFound} />
  </Router>
)
