import { Router, Route, IndexRoute } from 'react-router'
import React from 'react'
import history from 'common/history'

import PageNotFound from 'common/components/PageNotFound'
import AppContainer from './containers/AppContainer'
import Dashboard from './components/Dashboard'
import config from './config.json'

export default (
  <Router history={history}>
    <Route path={config.routes.admin} component={AppContainer}>
      <IndexRoute component={Dashboard} />
    </Route>    
    <Route path="*" component={PageNotFound} />
  </Router>
)
