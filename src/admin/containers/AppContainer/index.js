import React, { PropTypes } from 'react'
import { Provider } from 'react-redux'
import store from 'admin/store'

import ModuleMenu from 'admin/components/ModuleMenu'
import AdminModuleHeader from 'admin/components/AdminModuleHeader'
import ModulesNavigation from 'common/components/ModulesNavigation'
import AppConfigProvider from '../AppConfigProvider'
import withTranslate from 'app-config/decorators/withTranslate'
import './styles.css'

import { initToken } from 'admin/actions'
import { sessionGet } from 'common/utils'


class AppContainer extends React.Component {

  componentDidMount() {
    store.dispatch(initToken(sessionGet('token')))
  }

  render() {
    const { children } = this.props

    return (
      <Provider store={store}>
        <AppConfigProvider>
          <ModulesNavigation>
            <ModuleMenu />
            <AdminModuleHeader />
            <div className="ui divider"></div>
            {children}
          </ModulesNavigation>
        </AppConfigProvider>
      </Provider>
    )
  }
}

export default AppContainer
