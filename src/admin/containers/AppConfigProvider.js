import AppConfigProviderFactory from 'app-config/containers/AppConfigProviderFactory'
import { getAppConfig, isAppConfigLoading, isAppConfigErrorLoaded } from 'admin/reducers'
import config from 'admin/config.json'

export default AppConfigProviderFactory({
  getAppConfig,
  isAppConfigLoading,
  isAppConfigErrorLoaded,
  configUrl: config.configUrl
})
