import { combineReducers } from 'redux'
import appConfig, * as fromAppConfig from 'app-config/reducers'
import jwt, * as fromJwt from 'jwt/reducers'

export default combineReducers({
  appConfig,
  jwt
})

export const isAppConfigErrorLoaded = (state) => fromAppConfig.isErrorLoaded(state)

export const isAppConfigLoading = (state) => fromAppConfig.isLoading(state)

export const getAppConfig = (state) => fromAppConfig.getData(state)


export const getUsername = (state) => fromJwt.getUsername(state)

export const isAuthenticated = (state) => fromJwt.isAuthenticated(state)
