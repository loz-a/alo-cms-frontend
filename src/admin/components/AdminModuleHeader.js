import React, { PropTypes } from 'react'
import ModuleHeader from 'common/components/ModuleHeader'
import withTranslate from 'app-config/decorators/withTranslate'
import withNavigation from 'app-config/decorators/withNavigation'

const AdminModuleHeader = ({
  translate,
  navigation
}) => {
  const iconName = (navigation.activeItem || {}).icon || ''
  
  return (
    <ModuleHeader
      headerText={translate('Admin dashboard')}
      subheaderText={translate('Dashboard with general statistical information')}
      iconName={iconName}/>
  )
}


AdminModuleHeader.propTypes = {
  translate: PropTypes.func.isRequired
}


export default withNavigation(withTranslate(AdminModuleHeader))
