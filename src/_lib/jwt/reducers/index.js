import { JWT_INIT_TOKEN } from '../constants'

const INIT_STATE = {
  username: undefined
}

export default function(state = INIT_STATE, action) {

  switch (action.type) {
    case JWT_INIT_TOKEN:
      const { username } = action.payload
      if (username) return {
        username: username
      }

    default:
      return state
  }
}

export const getUsername = (state) => state.jwt.username

export const isAuthenticated = (state) => state.jwt.username !== undefined
