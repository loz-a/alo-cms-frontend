import jwtDecode from 'jwt-decode'
import { JWT_INIT_TOKEN } from 'jwt/constants'
import authConfig from 'auth/config.json'
import { redirect } from 'common/utils'

const initTokenSuccess = (username) => ({
  type: JWT_INIT_TOKEN,
  payload: { username }
})

export const initToken = (token) =>
  (dispatch) => {    
    if (token) dispatch(initTokenSuccess(jwtDecode(token).data.login))
    else redirect(authConfig.routes.login)
  }
