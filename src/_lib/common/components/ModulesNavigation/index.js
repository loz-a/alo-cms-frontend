import React, { PropTypes, Children, cloneElement } from 'react'
import jq from 'jquery'
import uiSidebar from 'semantic-ui-sidebar'
import './styles.css'

class ModulesNavigation extends React.Component {

  componentDidMount() {
    jq.fn.sidebar = uiSidebar
    const $sb = jq(this._sidebar)

    $sb.sidebar({
      transition: 'overlay',
      context: this._sidebar.parentNode
    })

    this.$sb = $sb
  }

  toggleVisibility = () => {
    this.$sb.sidebar('toggle')
  }

  render() {
    const { children } = this.props

    return (
      <div className="pushable">

        {this.getNavigation()}

        <div className="pusher ui container">
          { children }
        </div>
      </div>
    )
  }

  getNavigation = () => {
    const { appConfig } = this.props
    const navConfig = (appConfig.navigation && appConfig.navigation.modules) || []

    return (
      <nav className="ui sidebar vertical inverted menu" ref={(c) => this._sidebar = c}>
        {navConfig.map(
          (item, idx) => (
            <a  className="item" href={item.link} key={idx}>
              <i className={`${item.icon} icon`}></i> {item.title}
            </a>
          )
        )}
      </nav>
    )
  }

  getChildContext = () => ({
    mainNavigationToggleVisibility: this.toggleVisibility
  })

}

ModulesNavigation.propTypes = {
  appConfig: PropTypes.object,
  children: PropTypes.arrayOf(PropTypes.element.isRequired).isRequired
}

ModulesNavigation.childContextTypes = {
  mainNavigationToggleVisibility: PropTypes.func.isRequired
}

export default ModulesNavigation
