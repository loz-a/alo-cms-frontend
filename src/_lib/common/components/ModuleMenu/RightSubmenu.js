import React, { PropTypes } from 'react'
import { connect } from 'react-redux'
import withTranslate from 'app-config/decorators/withTranslate'
import { signOut } from 'auth/actions/signOut'

const RightSubmenu = (props, context) => {
  const { appConfig, translate, signOut } = props
  const { mainNavigationToggleVisibility } = context
  const menuItems = (appConfig.navigation && appConfig.navigation.module) || []

  return (
    <div className="right menu">
      <a className="item" onClick={mainNavigationToggleVisibility} key={0}>
        <i className="sidebar icon"></i> {translate('Main Menu')}
      </a>

      {menuItems.map(
        (item, idx) => item.routeName === 'logout'
          ? createSignOutItem(item, idx, signOutClickHandler(signOut))
          : createItem(item, idx)
      )}
    </div>
  )
}

function createItem(item, idx) {
  return (
    <a className="item" href={item.link} key={++idx}>
      <i className={`${item.icon} icon`}></i> {item.title}
    </a>
  )
}

function createSignOutItem(item, idx, signOutClickHandler) {
  return (
    <a className="item" href={item.link} key={++idx} onClick={signOutClickHandler}>
      <i className={`${item.icon} icon`}></i> {item.title}
    </a>
  )
}

const signOutClickHandler = (signOut) => (evt) => {
  evt.preventDefault()
  signOut()
}

RightSubmenu.propTypes = {
  appConfig: PropTypes.object.isRequired,
  translate: PropTypes.func.isRequired
}

RightSubmenu.contextTypes = {
  mainNavigationToggleVisibility: PropTypes.func
}

export default withTranslate(connect(null, {signOut})(RightSubmenu))
