import React, { cloneElement, createElement } from 'react'

class BaseTable extends React.Component {

  render() {    
    return (
      <table className={`ui ${this._getTableClassName()} table`}>
        {this._getTHead()}
        {this._getTBody()}
        {this._getTFoot()}
      </table>
    )
  }

  _getTHead() {
    throw new Error('Class must implement _getTHead() method')
  }

  _getTFoot = () => null

  _getTBody() {
    const { children } = this.props

    const items = this._getItems() || {}
    const itemElementPrototype = this._getItemElementPrototype()

    return (
      <tbody>
        {Object.keys(items).map((id) =>
           this._shouldDeleteItemRender(id)
            ? cloneElement(children, {key: id})
            : createElement(itemElementPrototype,
                { key: id, item: items[id], ...this._getItemProps(id)
              })
        )}
        {this._shouldAddItemRender() && this.props.children}
      </tbody>
    )
  }

  _getTableClassName = () => "bottom attached celled selectable small"

  _getItems = () => {}

  _getItemElementPrototype() {
    throw new Error('Class must implement _getItemElementPrototype() method')
  }

  _getItemProps = (itemId) => {}

  _shouldAddItemRender = () => false

  _shouldDeleteItemRender = (itemId) => false
}

export default BaseTable
