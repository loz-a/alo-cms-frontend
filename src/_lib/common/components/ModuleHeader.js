import React, { PropTypes } from 'react'
import { Header, Icon } from 'stardust'
// import Breadcrumbs from './Breadcrumbs'

const ModuleHeader = ({
  headerText,
  subheaderText,
  iconName="circle info"
}) => {
  return (
    <Header.H2>
      <Icon name={iconName} />
      <div className="content">
        {headerText}
        {/*  <Breadcrumbs /> */}

        {subheaderText &&
          <Header.Subheader>{subheaderText}</Header.Subheader>
        }
      </div>
    </Header.H2>
  )
}

ModuleHeader.propTypes = {
  headerText: PropTypes.string.isRequired,
  subheaderText: PropTypes.string,
  iconName: PropTypes.string
}

export default ModuleHeader
