import React, { Children, PropTypes } from 'react'
import { Table as BaseTable } from 'stardust'
import cx from 'classnames'

class Table extends BaseTable {

  constructor(...args) {
    super(...args)
    Table.propTypes['footerRenderer'] = PropTypes.func
  }

  _getCells(dataItem, rowIndex) {
    return Children.map(this.props.children, (column) => {
      let content
      if (column.props.cellRenderer) {
        content = column.props.cellRenderer(dataItem)
      } else {
        const itemContents = dataItem[column.props.dataKey]
        content = Table.getSafeCellContents(itemContents)
      }

      return (
        <td
          key={rowIndex + column.props.dataKey}
          className={column.props.className ? column.props.className : ''}
        >{content}</td>
      )
    })
  }

  _getFooter() {
    const { footerRenderer } = this.props
    if (!footerRenderer) return null
    return (
      <tfoot className="full-width">
        {footerRenderer()}
      </tfoot>
    )
  }

  render() {
    const {
      onSelectRow,
      onSortChange,
      defaultSelectedRows,
      sort,
      footerRenderer,
      ...other
    } = this.props

    const classes = cx(
      'ui',
      { selectable: !!onSelectRow || !!defaultSelectedRows },
      { sortable: !!onSortChange },
      this.props.className,
      'table'
    )

    return (
      <table {...other} className={classes}>
        <thead className="full-width">
          <tr>
            {this._getHeaders()}
          </tr>
        </thead>
        <tbody>
          {this._getRows()}
        </tbody>
        {this._getFooter()}
      </table>
    )
  }

}

export default Table
