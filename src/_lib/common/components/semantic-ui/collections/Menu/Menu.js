import React, { Children, cloneElement } from 'react'
import cx from 'classnames'
import { Menu as BaseMenu, MenuItem } from 'stardust'

class Menu extends BaseMenu {

  render() {
    const { activeItem, children, className, ...rest } = this.props

    const classes = cx('ui', className, 'menu')

    const _children = Children.map(children, (child) => {
      if (!child) return
      const { type, props } = child

      return type !== MenuItem ? child : cloneElement(child, {
        active: props.name === this.state.activeItem || props.name === activeItem,
        __onClick: this.handleClickItem,
      })
    })
    return (
      <div {...rest} className={classes}>
        {_children}
      </div>
    )
  }
}

export default Menu
