import React, { Children, cloneElement } from 'react'
import cx from 'classnames'
import { Menu, MenuItem } from 'stardust'

class RightMenu extends Menu {

  render() {
    const { activeItem, children, className, ...rest } = this.props
    const classes = cx(className, 'right', 'menu')

    const _children = Children.map(children, (child) => {
      const { type, props } = child
      return type !== MenuItem ? child : cloneElement(child, {
        active: props.name === this.state.activeItem || props.name === activeItem,
        __onClick: this.handleClickItem,
      })
    })

    return (
      <div {...rest} className={classes}>
        {_children}
      </div>
    )
  }
}

export default RightMenu
