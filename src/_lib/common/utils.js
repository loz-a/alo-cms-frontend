import axios from 'axios'

export function checkHttpStatus(response) {
    if (response.status >= 200 && response.status < 300) {
        return response
    } else {
        const error = new Error(response.statusText)
        error.response = response
        throw error
    }
}

export function redirect(route) {
  const token = sessionGet('token')
  const url = token ? `${route}?token=${token}` : route
  location.replace(url)
}

export function sessionSet(key, value) { sessionStorage.setItem(key, value) }

export function sessionGet(key) { return sessionStorage.getItem(key) }

export function sessionRemove (key) { sessionStorage.removeItem(key) }

export function httpClient() {
    const instance = axios.create();
    const jwtToken = sessionGet('token')

    if (jwtToken) {
        instance.defaults.headers.common['Authorization'] = `Bearer ${jwtToken}`;
    }
    return instance
}

export function isPromise(obj) {
  return !!obj && (typeof obj === 'object' || typeof obj === 'function') && typeof obj.then === 'function';
}
