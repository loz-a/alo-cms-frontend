import React, { PropTypes } from 'react'

export default (validator) =>
  (CustomComponent) => {

    class DecoratedComponent extends React.Component {

      state = {
        errorMessages: []
      }

      componentWillReceiveProps(nextProps) {        
        this.setState({ errorMessages: this.getErrorsFromObj(nextProps.validateErrors) })
      }

      validate = (data, context = undefined) => {
        if (!validator.validate) throw new Error('Validator must implement validate() method')

        const isValid = validator.validate(data, context) && !this.state.errorMessages.length

        if (isValid) {
          this.props.clearValidateErrors()
          return true
        }

        if (!validator.getErrorsAsArray) throw new Error('Validator must implement getErrorsAsArray() method')
        const errors = validator.getErrorsAsArray()

        // merge arrays with errors
        errors.push(...this.state.errorMessages)

        this.setState({ errorMessages: errors })

        return false
      }

      getErrorsFromObj = (errorsObj) => {
        const result = []

        for (const item in errorsObj) if (errorsObj.hasOwnProperty(item)) {
          result.push(...Object.values(errorsObj[item]))
        }
        return result
      }

      clearErrorsMessages = () => {
        this.setState({ errorMessages: [] })
        this.props.clearValidateErrors()
      }

      getErrorMessages = () => this.state.errorMessages

      render() {
        return <CustomComponent {...this.props}
          validate={this.validate}
          getErrorMessages={this.getErrorMessages}
          clearErrorsMessages={this.clearErrorsMessages} />
      }
  }

  DecoratedComponent.propTypes = {
    validateErrors: PropTypes.object.isRequired,
    clearValidateErrors: PropTypes.func.isRequired
  }

  return DecoratedComponent
}
