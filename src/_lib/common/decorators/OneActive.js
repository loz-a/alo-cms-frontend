import React from 'react'

export default (CustomComponent) =>
    class DecoratedComponent extends React.Component {
        state = {
            activeItem: null
        }

        toggleActive = (id) => () =>
            this.setState({
                activeItem: this.state.activeItem === id ? null : id
            })

        resetActive = () =>
            this.setState({
                activeItem: null
            })

        isActive = (id) => this.state.activeItem === id

        render() {
            return (
                <CustomComponent {...this.props}
                    toggleActive = {this.toggleActive}
                    isActive = {this.isActive}
                    resetActive = {this.resetActive}
                />
            )
        }
    }
