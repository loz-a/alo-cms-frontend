import { isPromise } from 'common/utils'

export const CALL_API = Symbol('Call API')

function isValidTypes(types) {
  return Array.isArray(types) && types.length === 3 && types.every((type) => typeof type === 'string')
}


export default (store) => (next) => (action) => {
  const callAPI = action[CALL_API]

  if (typeof callAPI === 'undefined') return next(action)

  const { types, apiResolver } = callAPI

  if (!isPromise(apiResolver)) {
    throw new Error('Specify apiResolver function.')
  }

  if (!isValidTypes(types)) {
    throw new Error('Expected an array of three action types and action types to be string')
  }

  function actionWith(data) {
    const enrichedAction = {
      ...action,
      ...data,
      payload: data.error || (data.response || {}).data || {}
    }
    delete enrichedAction[CALL_API]
    return enrichedAction
  }

  const [ REQUEST_TYPE, SUCCESS_TYPE, FAILURE_TYPE ] = types
  const { requestPayload } = callAPI

  next(
    actionWith({type: REQUEST_TYPE, payload: requestPayload})
  )

  return apiResolver
    .then(
      (response) => next(actionWith({
        type: SUCCESS_TYPE,
        response
      })),
      (error) => next(actionWith({
        type: FAILURE_TYPE,
        error: {
          errorMessage: error.message || 'Something was wrong'
        }
      }))
    )

}
