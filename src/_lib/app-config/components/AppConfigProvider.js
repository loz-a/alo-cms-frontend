import React, { PropTypes, Children, cloneElement } from 'react'
import { Message, Header } from 'stardust'

class AppConfigProvider extends React.Component {

  componentDidMount() {
    this.props.fetchConfig()
  }

  render() {
    const { appConfig, isLoading, isErrorLoaded, children } = this.props

    if (isLoading) return (
      <div className="ui active dimmer">
        <div className="ui big text loader">Loading</div>
      </div>
    )

    if (isErrorLoaded) return (
      <Message className="negative" icon="warning circle">
        <Header>
          An error occurred while loading data
        </Header>
       <p>Тry again or contact the developer</p>
     </Message>
    )

    const enrichedChildren = cloneElement(children, {appConfig})
    return Children.only(enrichedChildren)
  }

  getChildContext = () => ({
    appConfig: this.props.appConfig
  })

}

AppConfigProvider.propTypes = {
  children: PropTypes.element.isRequired
}

AppConfigProvider.childContextTypes = {
  appConfig: PropTypes.object.isRequired
}

export default AppConfigProvider
