import { combineReducers } from 'redux'
import {
  APP_CONFIG_REQUEST,
  APP_CONFIG_RECEIVE,
  APP_CONFIG_RECEIVE_FAILURE
} from '../constants'

const init = {
  data: {},
  loading: false,
  errorLoaded: false
}

function data(state = init.data, action) {
  switch (action.type) {
    case APP_CONFIG_RECEIVE:    
      if (action.response) {
        return {
          ...state,
          ...action.response
        }
      }
    default:
      return state
  }
}

function loading(state = init.loading, action) {
  switch (action.type) {
    case APP_CONFIG_REQUEST:
      return true
    case APP_CONFIG_RECEIVE:
    case APP_CONFIG_RECEIVE_FAILURE:
      return false
    default:
      return state
  }
}

function errorLoaded(state = init.errorLoaded, action) {
  switch (action.type) {
    case APP_CONFIG_REQUEST:
    case APP_CONFIG_RECEIVE:
      return false
    case APP_CONFIG_RECEIVE_FAILURE:
      return true
    default:
      return state
  }
}

const load = combineReducers({
  loading,
  errorLoaded
})

export default combineReducers({
  load,
  data
})

export const isErrorLoaded = (state) => state.appConfig.load.errorLoaded

export const isLoading = (state) => state.appConfig.load.loading

export const getData = (state) => state.appConfig.data
