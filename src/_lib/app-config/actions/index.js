import api from '../api'
import * as constants from '../constants'
import { checkHttpStatus, sessionGet } from 'common/utils'
import config from 'auth/config.json'

const requestConfig = () => ({
  type: constants.APP_CONFIG_REQUEST
})

const receiveConfig = (response) => ({
  type: constants.APP_CONFIG_RECEIVE,
  response
})

const receiveConfigFailure = (error) => ({
  type: constants.APP_CONFIG_RECEIVE_FAILURE,
  error
})

export const fetchConfig = (configUrl) => () =>
  (dispatch) => {
    dispatch(requestConfig())

    return api
      .fetchConfig(configUrl)
      .then(checkHttpStatus)
      .then(
        (response) => dispatch(receiveConfig(response.data || {})),
        (error) => {
          const responseStatus = (error.response || {}).status || 404

          if (responseStatus === 401 &&
            (config.routes && config.routes.login)
          ){
            location.replace(config.routes.login)
          }

          dispatch(receiveConfigFailure(error))
        }
      )
  }
