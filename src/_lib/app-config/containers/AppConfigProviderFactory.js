import { connect } from 'react-redux'
import AppConfigProvider from '../components/AppConfigProvider'
import { fetchConfig } from '../actions'

export default ({
  getAppConfig,
  isAppConfigLoading,
  isAppConfigErrorLoaded,
  configUrl
}) =>
  connect(
    (state) => ({
      appConfig: getAppConfig(state),
      isLoading: isAppConfigLoading(state),
      isErrorLoaded: isAppConfigErrorLoaded(state)
    }),
    {
      fetchConfig: fetchConfig(configUrl)
    }
  )(AppConfigProvider)
