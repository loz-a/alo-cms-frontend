/**
 * Mocking client-server processing
 */
 // import data from './mock.json'
 //
 // const TIMEOUT = 1200
 //
 // export default {
 //   fetchConfig(timeout = TIMEOUT) {
 //      // @todo mock reject
 //      return new Promise(
 //         (resolve, reject) => setTimeout(() => {
 //            resolve(data)
 //            // reject({
 //            //    error: 'something was wrong'
 //            // })
 //         }, timeout)
 //      )
 //   }
 // }

import { httpClient } from 'common/utils'

export default {
  fetchConfig(configUrl) {
    return httpClient().get(configUrl)
  }
}
