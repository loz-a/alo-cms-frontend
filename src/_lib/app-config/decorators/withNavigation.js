import React, { PropTypes } from 'react'

class Navigation {
  constructor(items) {
    this.items = items
  }

  set items(items) {
    if (!Array.isArray(items)) throw new Error('Setter expected array')
    this._items = items
  }

  get items() { return this._items }

  get activeItem() {
    const locPath = window.location.pathname
    const actives = this._items.filter((item) => item.link ===locPath)
    return actives.length ? actives[0] : null
  }

  *[Symbol.iterator]() {
    const items = this._items
    for (let i=0; i<items.length; i++) {
      yield items[i]
    }
  }

}

export default function withNavigation(WrappedComponent) {

  const WithNavigation = (props, context) => {
    const ac = props.appConfig || context.appConfig || {}
    const items = (ac.navigation || ac.navigation || {}).modules || []

    const newProps = {
      ...props,
      navigation: new Navigation(items)
    }

    return React.createElement(WrappedComponent, newProps)
  }

  WithNavigation.propTypes = {
    appConfig: PropTypes.object
  }

  WithNavigation.contextTypes = {
    appConfig: PropTypes.object
  }

  WithNavigation.WrappedComponent = WrappedComponent

  return WithNavigation
}
