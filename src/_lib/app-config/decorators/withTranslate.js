import React, { PropTypes } from 'react'

export default function withTranslate(WrappedComponent) {

  const WithTranslate = (props, context) => {
    const appConfig = props.appConfig || context.appConfig || {}
    const translations = appConfig.translations || {}

    const translate = (key) => translations[key] ? translations[key] : key

    const newProps = Object.assign({}, props, { translate })
    return React.createElement(WrappedComponent, newProps)
  }

  WithTranslate.propTypes = {
    appConfig: PropTypes.object
  }

  WithTranslate.contextTypes = {
    appConfig: PropTypes.object
  }

  WithTranslate.WrappedComponent = WrappedComponent

  return WithTranslate
}
