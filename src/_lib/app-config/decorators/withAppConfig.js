import React, { PropTypes } from 'react'

export default function withAppConfig(WrappedComponent) {

  const WithAppConfig = (props, context) => {
    const appConfig = props.appConfig || context.appConfig
    const newProps = Object.assign({}, props, {appConfig})

    return React.createElement(WrappedComponent, newProps)
  }

  WithAppConfig.propTypes = {
    appConfig: PropTypes.object
  }

  WithAppConfig.contextTypes = {
    appConfig: PropTypes.object
  }

  WithAppConfig.WrappedComponent = WrappedComponent

  return WithAppConfig
}
