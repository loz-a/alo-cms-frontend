import React from 'react'
import { Router, Route } from 'react-router'
import history from 'common/history'

import PageNotFound from 'common/components/PageNotFound'
import AppContainer from './containers/AppContainer'
import config from './config.json'

export default (
  <Router history={history}>
    <Route path={config.routes.translations} component={AppContainer}>
      {/* <IndexRoute component={Dashboard} /> */}
    </Route>
    <Route path="*" component={PageNotFound} />
  </Router>
)
