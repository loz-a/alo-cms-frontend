const path = require('path')
const webpack = require('webpack')
const ExtractTextPlugin = require('extract-text-webpack-plugin')

module.exports = {
  devtool: 'cheap-inline-module-source-map',
  entry: {
      // 'site-options': './src/site-options/index.js',
      'admin': './src/admin/index.js',
      'auth': './src/auth/index.js',
      'translations': './src/translations/index.js',
      'languages': './src/languages/index.js'
  },

  output: {
      path: path.join(__dirname, 'public'),
      filename: '../../public/resources/[name]/[name].bundle.js',
      publicPath: '/'
  },

  module: {
      loaders: [
          {
              test: /\.json$/,
              exclude: /(node_modules|bower_components)/,
              loader: 'json-loader',
          }, {
              test: /\.css$/,
              loader: ExtractTextPlugin.extract('css')
          }, {
              test: /\.(png|jpg|svg|ttf|eot|woff|woff2)$/,
              loader: 'url?name=[path][name].[ext]&limit=4096'
          }
      ]
  },

  plugins: [
        new ExtractTextPlugin('../../public/resources/[name]/[name].css', {allChunks: true}),
        new webpack.optimize.OccurenceOrderPlugin(),
        new webpack.optimize.DedupePlugin(),
        new webpack.NoErrorsPlugin()
  ],

  resolve: {
        modulesDirectories: ['node_modules'],
        extensions: ['', '.js', '.json', '.css'],
        alias: {
            'common': path.resolve(__dirname, '../src/_lib/common'),
            'app-config': path.resolve(__dirname, '../src/_lib/app-config'),
            'jwt': path.resolve(__dirname, '../src/_lib/jwt'),
            'site-options': path.resolve(__dirname, '../src/site-options'),
            'admin': path.resolve(__dirname, '../src/admin'),
            'auth': path.resolve(__dirname, '../src/auth'),
            'translations': path.resolve(__dirname, '../src/translations'),
            'languages': path.resolve(__dirname, '../src/languages')
        }
    },

    resolveLoader: {
        modulesDirectories: ['node_modules'],
        moduleTemplates: ['*-loader', '*'],
        extensions: ['', '.js']
    }
}
