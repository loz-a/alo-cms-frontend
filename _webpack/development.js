const path = require('path')
const webpack = require('webpack')
const baseConfig = require('./_base')
const proxy = require('./development/proxy')

baseConfig.module.loaders.push({
    test: /\.js$/,
    exclude: /(node_modules|bower_components)/,
    loader: 'babel-loader',
    query: {
        plugins: ['transform-runtime'],
        presets: ["react", "es2015", "es2017", "react-hmre", "babel-preset-stage-0"]
    }
})

baseConfig.plugins.push(
    new webpack.DefinePlugin({
        DEVELOPMENT: JSON.stringify(true),
        'process.env': {
            'NODE_ENV': JSON.stringify('development')
        }
    })
)

baseConfig.devServer = {
    contentBase: path.join(__dirname, '../public'),
    historyApiFallback: {
        rewrites: [
            { from: /^\/site-options/, to: '/site-options.html' },
            { from: /^\/admin/, to: '/admin.html' },
            { from: /^\/login/, to: '/auth.html' },
            { from: /^\/translations/, to: '/translations.html' },
            { from: /^\/languages/, to: '/languages.html' }
        ],
    },
    host: 'localhost',
    port: 8082,
    proxy: proxy
}

module.exports = baseConfig
