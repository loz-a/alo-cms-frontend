const path = require('path')
const webpack = require('webpack')
const ExtractTextPlugin = require('extract-text-webpack-plugin')

module.exports = {
    devtool: 'cheap-inline-module-source-map',
    entry: {
        'site-options': './src/site-options/index.js',
        'admin': './src/admin/index.js'
    },

    output: {
        path: path.join(__dirname, 'public'),
        filename: 'resources/[name]/[name].bundle.js',
        publicPath: '/'
    },

    module: {
        loaders: [
            {
                test: /\.js$/,
                exclude: /(node_modules|bower_components)/,
                loader: 'babel-loader',
                query: {
                    presets: ["react", "es2015", "react-hmre", "babel-preset-stage-0"]
                }
            }, {
                test: /\.json$/,
                exclude: /(node_modules|bower_components)/,
                loader: 'json-loader',
            }, {
                test: /\.css$/,
                loader: ExtractTextPlugin.extract('css')
            },
            {
                test: /\.(png|jpg|svg|ttf|eot|woff|woff2)$/,
                loader: 'url?name=[path][name].[ext]&limit=4096'
            }
        ]
    },

    plugins: [
        new ExtractTextPlugin('resources/[name]/[name].css', {allChunks: true}),
        new webpack.optimize.OccurenceOrderPlugin(),
        new webpack.DefinePlugin({
            DEVELOPMENT: JSON.stringify(true),
            'process.env': {
                'NODE_ENV': JSON.stringify('development')
            }
        }),
        new webpack.NoErrorsPlugin()
    ],

    resolve: {
        modulesDirectories: ['node_modules'],
        extensions: ['', '.js', '.json', '.css'],
        alias: {
            common: path.resolve(__dirname, 'src/common'),
            'site-options': path.resolve(__dirname, 'src/site-options'),
            'admin': path.resolve(__dirname, 'src/admin')
        }
    },

    resolveLoader: {
        modulesDirectories: ['node_modules'],
        moduleTemplates: ['*-loader', '*'],
        extensions: ['', '.js']
    },

    devServer: {
        contentBase: path.join(__dirname, '/public'),
        historyApiFallback: {
            rewrites: [
                { from: /^\/site-options/, to: '/site-options.html' },
                { from: /^\/admin/, to: '/admin.html' }
            ],
        },
        host: 'localhost',
        port: 8082,
        proxy: {
            "/app-config.json": {
              target: 'http://alo-cms.local:8080/app-config.json',
              ignorePath: true,
              changeOrigin: true,
              secure: false
            }
        }
    }
}
